#include <gzc/games/rpg/core/character/race/Elf.hpp>
#include <gzc/games/rpg/core/character/characteristics/Force.hpp>
#include <gzc/games/rpg/core/character/characteristics/Dexterity.hpp>
#include <gzc/games/rpg/core/character/characteristics/Constitution.hpp>
#include <gzc/games/rpg/core/character/characteristics/Intelligence.hpp>
#include <gzc/games/rpg/core/character/characteristics/Wisdom.hpp>
#include <gzc/games/rpg/core/character/characteristics/Charisma.hpp>

#include <gzc/logger/Logger.hpp>

#include <iostream>

using namespace rpg::character;
using namespace util;

int main( int argc, char *argv[] )
{
    Logger logger( "main", "rpg.log", true );
    logger.log( Logger::DEBUG, "Calling: " );
    for( int i = 0; i < argc; i++ )
    {
        logger.log( Logger::DEBUG, argv[ i ] );
    }
    logger.log( Logger::INFO, "RPG Core by Giorgio Caculli" );

    int res = 0;

    try
    {
        Character character = race::Elf(
                "Seldon Gelleger",
                true,
                92,
                race::SkinTone::GREEN,
                1.95f,
                80.f,
                race::Color::GREEN,
                race::Color::GREEN,
                "Bari",
                "April",
                "Loyal" );

        auto *force = new characteristics::Force( 10 );
        auto *dexterity = new characteristics::Dexterity( 11 );
        auto *constitution = new characteristics::Constitution( 12 );
        auto *intelligence = new characteristics::Intelligence( 13 );
        auto *wisdom = new characteristics::Wisdom( 14 );
        auto *charisma = new characteristics::Charisma( 15 );

        std::array< characteristics::Characteristic *, characteristics::Characteristic::CHARACTERISTICS_COUNT > characteristics =
                {
                        force, dexterity, constitution, intelligence, wisdom, charisma
                };

        character.set_characteristics( characteristics );

        logger.log( Logger::INFO, character.to_string() );

        for ( characteristics::Characteristic *characteristic: characteristics )
        {
            delete characteristic;
        }
    } catch (std::exception &e )
    {
        res = -1;
    }

    return res;
}
