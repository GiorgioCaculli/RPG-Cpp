#include <gzc/games/rpg/core/character/Character.hpp>

#include <sstream>
#include <utility>

using namespace rpg::character;

Character::Character( std::string name, bool gender, int age, race::SkinTone skin_tone, float height, float weight, race::Color hair_color, race::Color eye_color, const std::string &birth_place, const std::string &birth_date, std::string alignment )
        : Race( gender, age, skin_tone, height, weight, hair_color, eye_color, birth_place, birth_date )
        , _name( std::move( name ) )
        , _alignment( std::move( alignment ) )
        , _experience( 0 )
        , _characteristics()
{
}

const std::string &Character::get_name() const
{
    return _name;
}

void Character::set_name( const std::string &name )
{
    Character::_name = name;
}

const std::string &Character::get_alignment() const
{
    return _alignment;
}

void Character::set_alignment( const std::string &alignment )
{
    Character::_alignment = alignment;
}

int Character::get_experience() const
{
    return _experience;
}

void Character::set_experience( int experience )
{
    _experience = experience;
}

const std::array< characteristics::Characteristic *, characteristics::Characteristic::CHARACTERISTICS_COUNT > &Character::get_characteristics() const
{
    return _characteristics;
}

void Character::set_characteristics( const std::array< characteristics::Characteristic *, characteristics::Characteristic::CHARACTERISTICS_COUNT > &characteristics )
{
    _characteristics = characteristics;
}

std::string Character::to_string() const
{
    std::stringstream ss;
    ss <<
       "Name: " << _name << std::endl <<
       //static_cast< const race::Race & >( character ) << std::endl <<
       "Alignment: " << _alignment << std::endl <<
       "Experience: " << _experience << std::endl;
    for ( characteristics::Characteristic *characteristic: _characteristics )
    {
        ss << characteristic->get_name() << " : " << characteristic->get_value() << std::endl;
    }
    return ss.str();
}
