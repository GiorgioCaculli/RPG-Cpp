#include <gzc/games/rpg/core/character/characteristics/Charisma.hpp>

using namespace rpg::character::characteristics;

Charisma::Charisma( short value )
        : Characteristic( CHARISMA, value )
{
    set_name( "Charisma" );
}

Charisma::Charisma( const Charisma &charisma )
        : Charisma( charisma.get_value() )
{

}

Charisma &Charisma::operator=( const Charisma &charisma )
{
    if ( &charisma != this )
    {
        Characteristic::operator=( charisma );
    }
    return *this;
}

Charisma::~Charisma()
= default;
