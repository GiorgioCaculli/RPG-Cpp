#include <gzc/games/rpg/core/character/characteristics/Dexterity.hpp>

using namespace rpg::character::characteristics;

Dexterity::Dexterity( short value )
        : Characteristic( DEXTERITY, value )
{
    set_name( "Dexterity" );
}

Dexterity::Dexterity( const Dexterity &dexterity )
        : Dexterity( dexterity.get_value() )
{
}

Dexterity &Dexterity::operator=( const Dexterity &dexterity )
{
    if ( &dexterity != this )
    {
        Characteristic::operator=( dexterity );
    }
    return *this;
}

Dexterity::~Dexterity()
= default;
