#ifndef RPG_CHARACTER_FORCE_HPP
#define RPG_CHARACTER_FORCE_HPP

#include <gzc/games/rpg/core/character/characteristics/Characteristic.hpp>

namespace rpg
{
    namespace character
    {
        namespace characteristics
        {
            class Force
                    : public Characteristic
            {
            public:
                explicit Force( short value );
                Force( const Force &force );
                Force &operator=( const Force &force );
                ~Force() override;
            };
        }
    }
}

#endif //RPG_CHARACTER_FORCE_HPP
