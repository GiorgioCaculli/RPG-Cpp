#ifndef RPG_CHARACTER_INTELLIGENCE_HPP
#define RPG_CHARACTER_INTELLIGENCE_HPP

#include <gzc/games/rpg/core/character/characteristics/Characteristic.hpp>

namespace rpg
{
    namespace character
    {
        namespace characteristics
        {
            class Intelligence
                    : public Characteristic
            {
            public:
                explicit Intelligence( short value );
                Intelligence( const Intelligence &intelligence );
                Intelligence &operator=( const Intelligence &intelligence );
                ~Intelligence() override;
            };
        }
    }
}

#endif //RPG_CHARACTER_INTELLIGENCE_HPP
