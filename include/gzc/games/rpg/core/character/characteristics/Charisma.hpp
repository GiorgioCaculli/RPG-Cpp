#ifndef RPG_CHARACTER_CHARISMA_HPP
#define RPG_CHARACTER_CHARISMA_HPP

#include <gzc/games/rpg/core/character/characteristics/Characteristic.hpp>

namespace rpg
{
    namespace character
    {
        namespace characteristics
        {
            class Charisma
                    : public Characteristic
            {
            public:
                explicit Charisma( short value );
                Charisma( const Charisma &charisma );
                Charisma &operator=( const Charisma &charisma );
                ~Charisma() override;
            };
        }
    }
}

#endif //RPG_CHARACTER_CHARISMA_HPP
