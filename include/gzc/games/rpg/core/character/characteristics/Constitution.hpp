#ifndef RPG_CHARACTER_CONSTITUTION_HPP
#define RPG_CHARACTER_CONSTITUTION_HPP

#include <gzc/games/rpg/core/character/characteristics/Characteristic.hpp>

namespace rpg
{
    namespace character
    {
        namespace characteristics
        {
            class Constitution
                    : public Characteristic
            {
            public:
                explicit Constitution( short value );
                Constitution( const Constitution &constitution );
                Constitution &operator=( const Constitution &constitution );
                ~Constitution() override;
            };
        }
    }
}

#endif //RPG_CHARACTER_CONSTITUTION_HPP
