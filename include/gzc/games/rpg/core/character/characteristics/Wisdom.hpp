#ifndef RPG_CHARACTER_WISDOM_HPP
#define RPG_CHARACTER_WISDOM_HPP

#include <gzc/games/rpg/core/character/characteristics/Characteristic.hpp>

namespace rpg
{
    namespace character
    {
        namespace characteristics
        {
            class Wisdom
                    : public Characteristic
            {
            public:
                explicit Wisdom( short value );
                Wisdom( const Wisdom &wisdom );
                Wisdom &operator=( const Wisdom &wisdom );
                ~Wisdom() override;
            };
        }
    }
}

#endif //RPG_CHARACTER_WISDOM_HPP
