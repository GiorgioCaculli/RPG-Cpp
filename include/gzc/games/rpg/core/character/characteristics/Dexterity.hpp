#ifndef RPG_CHARACTER_DEXTERITY_HPP
#define RPG_CHARACTER_DEXTERITY_HPP

#include <gzc/games/rpg/core/character/characteristics/Characteristic.hpp>

namespace rpg
{
    namespace character
    {
        namespace characteristics
        {
            class Dexterity
                    : public Characteristic
            {
            public:
                explicit Dexterity( short value );
                Dexterity( const Dexterity &dexterity );
                Dexterity &operator=( const Dexterity &dexterity );
                ~Dexterity() override;
            };
        }
    }
}

#endif //RPG_CHARACTER_DEXTERITY_HPP
